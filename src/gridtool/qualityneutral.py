#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# This file is part of gridtool.
#
# gridtool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gridtool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with gridtool.  If not, see <http://www.gnu.org/licenses/>.

import sys, warnings, os
import numpy as np
from os.path import exists, join as pjoin
from array import array
from Block import Block
from QualityTaskNeutral import QualityTask
import Quality

measures = Quality.getModules()

# Parse arguments
inDir = 'Grid'
outDir = 'GridReport'
blocks = []
fluidy = False

skip = 0
for i, arg in enumerate(sys.argv[2:], 2):
    if skip:
        skip -= 1
        continue
    
    if arg == '-i':
        skip = 1
        inDir = sys.argv[i+1]
        continue
    
    if arg == '-o':
        skip = 1
        outDir = sys.argv[i+1]
        continue
        
    if arg == '--fluidy':
        fluidy = True
        continue
    
    blocks.append(int(arg))


if not exists(inDir):
    print 'Input directory does not exist.'
    print 'Please specify it with "-i dir".'
    print 'Exiting.'
    exit(1)

try:    
    os.mkdir(outDir)
except:
    pass

# Fluidy - parallel check
if fluidy:
    myhostname = os.uname()[1].split('.', 1)[0]
    
    # Changing stdout
    sys.stdout = open('gridtool.out.%s' % myhostname, 'w', 0)
    
    # Read hostfile
    fs = open('/home/arteagaa/hostfile')
    hosts = fs.read().split('\n')[:-1]
    fs.close()
    
    if myhostname not in hosts:
        exit(0)

    procN = len(hosts)
    rank = hosts.index(myhostname)

    # Getting number of blocks
    bN = 1
    while True:
        if not exists(pjoin(inDir, 'block.%i' % bN)):
            bN -= 1
            break
        bN += 1

    # Getting my blocks
    myblocks = []
    b = rank+1
    while b <= bN:
        myblocks.append(b)
        b += procN
    
    blocks = myblocks
    
    # Print -- to file
    print "Machine %s will check blocks" % myhostname, blocks

# Do computation
qt = QualityTask(inDir, outDir, blocks)
qt.run(measures)
