#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# This file is part of gridtool.
#
# gridtool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gridtool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with gridtool.  If not, see <http://www.gnu.org/licenses/>.

from Block import Block
import numpy as np

class QualityTask:
    def __init__(self, dbhash, db, *args):
        self.db = db
        self.savedir = 'Report/' + dbhash + '/'
        
        # Get blocks ids
        blocks = []
        for i in args:
            if type(i) == type(0):
                blocks.append(i)
            else:
                blocks += i
                
        # Uniquify
        s = set(blocks)
        self.blocks = list(s)
        
        # Get number of characters
        l = self.blocks[-1]
        k = int(np.floor(np.log10(l))) + 1
        self.msg = ' -- Measuring block %%%ii/%i : %%ix%%ix%%i' % (k, l)
        
        
    def run(self, measures):
        result = {}
        
        for bid in self.blocks:
             
            # Get n1, n2, n3
            temprt = self.db['MDES.' + str(bid)][:]
            ns = tuple([temprt[s][0]+1 for s in ('N1', 'N2', 'N3')])
            n1, n2, n3 = ns
            print self.msg % ((bid, ) + ns)
            
            # Get coordinates and construct block
            coordinates = self.db['COOR.' + str(bid)][:].reshape(n1*n2*n3, 3)
            b = Block(bid, ns, coordinates)
            
            # Measure
            parres = {}
            for m in measures:
                print '   -- Module ' + m.__name__ + ' on block ' + str(bid)
                
                # Compute 
                parres.update(m.measure(b, self.savedir))
            print
            
            # Concatenate results
            for k, v in parres.items():
                if k in result:
                    result[k]['values'] = \
                        np.concatenate((result[k]['values'], v['values']))
                    result[k]['bad'] += v['bad']
                    result[k]['bb'].append(v['bb'])
                else:
                    result[k] = v
                    result[k]['bb'] = [result[k]['bb']]
                    
        return result
