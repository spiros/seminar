#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# This file is part of gridtool.
#
# gridtool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gridtool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with gridtool.  If not, see <http://www.gnu.org/licenses/>.

import sys, os
from os.path import join as pjoin

# Loading MEMCOM module
if not 'MEMCOM' in os.environ:
    print "MEMCOM module not loaded!"
    print "Trying with /opt/software/smr/"
    memcompath = '/opt/software/smr/'
else:
    memcompath = os.environ['MEMCOM']
    
sys.path.append(pjoin(memcompath, 'lib', 'python2.5', 'site-packages'))
sys.path.append(pjoin(memcompath, 'lib', 'python2.6', 'site-packages'))

import memcom
