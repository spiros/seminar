#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# This file is part of gridtool.
#
# gridtool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gridtool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with gridtool.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np

def parse(self):
    parse_coor(self)
    parse_mdes(self)
    
    
def parse_coor(self):
    print "Opening coordinates file " + self.infile
    fs = file(self.infile, 'r')
    blocks = []
    blockdims = []
    bn = 0
    nn = 0

    while True:
        l = fs.readline()
        
        # End file
        if len(l) == 0:
            print "File end reached"
            print
            break
            
        # Comment
        if l[0] == '#':
            continue

        # Read block dimensions
        n1, n2, n3 = [int(i) for i in l.split()]
        
        print " -- Reading %ix%ix%i block" % (n1,n2,n3)
        curblock = {}
        blockdims.append((n1,n2,n3))

        # Read coordinates
        for i,j,k in [(ii,jj,kk) for ii in xrange(n1)
                                 for jj in xrange(n2)
                                 for kk in xrange(n3)]:
            l = fs.readline()
            curblock[(i,j,k)] = [float(c) for c in l.split()]

        # Reorder coordinates
        bn = 0
        curblockOrdered = np.zeros((n1*n2*n3, 3))
        
        for k in xrange(n3):
            for j in xrange(n2):
                for i in xrange(n1):
                    curblockOrdered[bn, :] = curblock[(i, j, k)]
                    bn += 1
        

        # Store block
        blocks.append(curblockOrdered)

        
    # Save blocks into the database
    for i,b in enumerate(blocks, 1):
        setname = 'COOR.%i' % i
        self.db[setname] = b
    self.db.sync()

    self.blockdims = blockdims
    
    
def parse_mdes(self):
    print "Opening connectivity file " + self.infile_conn
    fs = file(self.infile_conn, 'r')
    while True:
        l = fs.readline()

        # File is void
        if len(l) == 0:
            print "!! Error: file " +self.infile_conn+ " has no information !!"
            return

        # Comment
        if l[0] == '#':
            continue

        l = l.split()
        if l[1] == 'blocks':
            nblocks = int(l[0])
            break

    print " -- Reading %i blocks from file" % (nblocks)

    bid = 0
    while bid < nblocks:
        l = fs.readline()

        # End file : error
        if len(l) == 0:
            print "!! Unexpected end of file !!"
            return

        # Comment
        if l[0] == '#':
            continue


        # New block
        bid += 1
        print " -- Block #%i" % bid
        
        n1,n2,n3 = [bd-1 for bd in self.blockdims[bid-1]]
        nn = n1*n2*n3
        mshwdf = (1, n2, 1, n3, 1, n2, 1, n3,
                  1, n3, 1, n1, 1, n3, 1, n2,
                  1, n1, 1, n1, 1, n1, 1, n2)

        mshwbc = []
        mshwcf = []
        mshwcd = []
        blkcon = []
        blkfac = []
        blkdir = []
        blkdef = []

        data = l.split()[2:-1]
        
        # fid is the faceid (-i, +i, -j, +j, -k, +k)
        for i in xrange(6):
            fid = i+1
            t = data[4*i]

            # Surface
            if t == 's':
                mshwbc.append(300)
                print " ---- Block %i, face %i connected to wall %i" % \
                  (bid, fid, int(data[4*i+1]))
                continue

            # Block connectivity
            if (t == 'b') or (t == 'p' and bid != int(data[4*i + 2])):
                mshwbc.append(500)
                blkcon.append(int(data[4*i+2]))
                blkfac_, blkdir_ = getBlkinfo(fid, int(data[4*i+3]))
                blkfac.append(blkfac_)
                blkdir += blkdir_
                blkdef += mshwdf[4*i : 4*(i+1)]
                print \
                  " ---- Block %i, face %i connected to block %i, face %i;" % \
                  (bid, fid, int(data[4*i+2]), blkfac_),\
                  "blkdir: %i %i" % tuple(blkdir_)
                continue
                

            if t == 'p':
                mshwbc.append(999)
                bid2 = int(data[4*i + 2])
                # Add periodic boundary condition
                mshwbc.append(430)
                mshwcf.append(fid)
                mshwcd += mshwdf[4*(fid-1) : 4*fid]
                continue
                

        mdes = dict(N1=n1, N2=n2, N3=n3, NN=nn, MSHWBC=mshwbc, MSHWDF=mshwdf)

        if mshwcf:
            mdes['MSHWCF'] = mshwcf
        if mshwcd:
            mdes['MSHWCD'] = mshwcd

        if blkcon:
            mdes['BLKCON'] = blkcon
        if blkfac:
            mdes['BLKFAC'] = blkfac
        if blkdef:
            mdes['BLKDEF'] = blkdef
        if blkdir:
            mdes['BLKDIR'] = blkdir
        
        mdesname = 'MDES.%i' % bid
        self.db[mdesname] = mdes
        self.db.sync()
        print
    print "File end reached"


def getBlkinfo(faceid, code):
    c1, c2, c3 = code/100, code/10%10, code%10
    decode = lambda(c) : (-1)**(c/4) * ((c-1)%3+1)
    
    if faceid % 2 == 0:
        perm = (0, 1, 3, 5, 2, 4, 6)
    else:
        perm = (0, 2, 4, 6, 1, 3, 5)
    
    if faceid in (1, 2):
        return perm[c1], [decode(c2), decode(c3)]
    if faceid in (3, 4):
        return perm[c2], [decode(c3), decode(c1)]
    if faceid in (5, 6):
        return perm[c3], [decode(c1), decode(c2)]


class Parser:
    def __init__(self, infile, infile_conn, db):
        self.infile = infile
        self.infile_conn = infile_conn
        self.db = db
        parse(self)
