#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# This file is part of gridtool.
#
# gridtool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gridtool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with gridtool.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np, numpy.linalg as la
from utils import loadValues, saveValues, loadBad, saveBad


exptol = 4.

def measure(block, savepath=None):
    nn = (block.n1-2) * (block.n2-2) * (block.n3-2)
    bid = block.bid
    loaded = False
    
    try:
        exps = loadValues('%s/exp.%i' % (savepath, block.bid))
        badexps = loadBad('%s/badexp.%i' % (savepath, block.bid))
        
        loaded = True
    
    except:
        exps = np.zeros(nn)
        badexps = []
        
        n = 0
        for i in xrange(1, block.n1-1):
            for j in xrange(1, block.n2-1):
                for k in xrange(1, block.n3-1):
                    val = _volumeRatio(block, i, j, k)
                    exps[n] = val
                    
                    if val > exptol:
                        badexps.append((bid, i, j, k, val))
                        
                    n += 1
                
    ret = dict(
        exp   = {'descr':'Cell expansion', 'values':exps, \
                  'bad':badexps, 'bb':len(badexps) }
    )
    
    # Save values
    if savepath is not None and not loaded:
        saveValues('%s/exp.%i' % (savepath, block.bid), exps)
        saveBad('%s/badexp.%i' % (savepath, block.bid), badexps)
    
    return ret
    
    


# Internal usage variables and functions

_volumesMatrix = [
    (0,2,4), (0,2,5),  (0,3,4), (0,3,5),
    (1,2,4), (1,2,5),  (1,3,4), (1,3,5)
]
    
def _volumeRatio(block, i, j, k):
    if not all((i>0, i<block.n1-1, j>0, j<block.n2-1, k>0, k<block.n3-1)):
        raise Exception \
              ("Invalid point for volume check: (%i, %i, %i)" % (i, j, k))

    c = block.coordinates
    p = block.coordinates[i, j, k, :]
    
    # Store vectors with following order:
    # -i, +i, -j, +j, -k, +k
    lv = [p1 - p for p1 in [
        c[i-1, j, k, :], c[i+1, j, k, :],
        c[i, j-1, k, :], c[i, j+1, k, :],
        c[i, j, k-1, :], c[i, j, k+1, :]
    ]]
    
    vmin, vmax = np.Inf, 0.
    for a1, a2, a3 in _volumesMatrix:
        D = np.array((lv[a1], lv[a2], lv[a3]))
        Ddet = np.abs(la.det(D))
        vmin = min(vmin, Ddet)
        vmax = max(vmax, Ddet)
    
    return vmax/vmin
