#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# This file is part of gridtool.
#
# gridtool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gridtool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with gridtool.  If not, see <http://www.gnu.org/licenses/>.

from os.path import exists
from array import array
import numpy as np

def loadValues(path):
    fs = open(path, 'rb')
    
    # Load size
    a = array('L')
    a.fromfile(fs, 1)
    size = a.pop()
    
    # Load values
    b = array('d')
    b.fromfile(fs, size)
    
    # Close file and return
    fs.close()
    return np.array(b.tolist())
    
    
def saveValues(path, values):
    values = values.tolist()
    fs = open(path, 'wb')
    
    # Store size
    size = len(values)
    a = array('L', [size])
    a.tofile(fs)
    
    # Store values
    b = array('d', values)
    b.tofile(fs)
    
    # Close
    fs.close()
    
    
def loadBad(path):
    fs = open(path, 'rb')
    
    # Load size
    a = array('L')
    a.fromfile(fs, 1)
    size = a.pop()
    
    # Load data
    bidArray = array('L')
    bidArray.fromfile(fs, size)
    bid = bidArray.tolist()
    
    iArray = array('L')
    iArray.fromfile(fs, size)
    i = iArray.tolist()
    
    jArray = array('L')
    jArray.fromfile(fs, size)
    j = jArray.tolist()
    
    kArray = array('L')
    kArray.fromfile(fs, size)
    k = kArray.tolist()
    
    valArray = array('d')
    valArray.fromfile(fs, size)
    val = valArray.tolist()
    
    # Close file and return
    fs.close()
    
    ret = []
    for t in xrange(size):
        ret.append((bid[t], i[t], j[t], k[t], val[t]))
    
    return ret
    
def saveBad(path, values):
    size = len(values)
    fs = open(path, 'wb')
    
    # Store size
    size = len(values)
    a = array('L', [size])
    a.tofile(fs)
    
    # Prepare data
    bidList = [l[0] for l in values]
    iList   = [l[1] for l in values]
    jList   = [l[2] for l in values]
    kList   = [l[3] for l in values]
    valList = [l[4] for l in values]
    
    # Store data
    array('L', bidList).tofile(fs)
    array('L', iList).tofile(fs)
    array('L', jList).tofile(fs)
    array('L', kList).tofile(fs)
    array('d', valList).tofile(fs)
    
    # Close
    fs.close()
