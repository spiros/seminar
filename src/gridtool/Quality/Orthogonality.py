#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# This file is part of gridtool.
#
# gridtool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gridtool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with gridtool.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from utils import loadValues, saveValues, loadBad, saveBad

descr = dict(
    amins   = 'Minimal angle',
    amaxs   = 'Maximal angle',
    aratios = 'Maximal/Minimal angle ratio'
)


# Define tolerance for bad cells
# These should be 30., 145., 4.
amintol, amaxtol, aratiotol = 30., 145., 4.


def measure(block, savepath=None):
    nn = (block.n1-1) * (block.n2-1) * (block.n3-1)
    bid = block.bid
    loaded = False
    
    try:
        amins = loadValues('%s/amin.%i' % (savepath, block.bid))
        amaxs = loadValues('%s/amax.%i' % (savepath, block.bid))
        aratios = loadValues('%s/aratio.%i' % (savepath, block.bid))
        
        badamins = loadBad('%s/badamin.%i' % (savepath, block.bid))
        badamaxs = loadBad('%s/badamax.%i' % (savepath, block.bid))
        badaratios = loadBad('%s/badaratio.%i' % (savepath, block.bid))
        
        loaded = True
    
    except:

        amins = np.zeros(nn)
        amaxs = np.zeros(nn)
        aratios = np.zeros(nn)

        badamins = []
        badamaxs = []
        badaratios = []
        
        n = 0
        t = 180./np.pi
        for i in xrange(block.n1-1):
            for j in xrange(block.n2-1):
                for k in xrange(block.n3-1):
                    h = block.getHexahedron(i, j, k)
                    amin, amax = h.getAnglesMinMax()
                    amin, amax = amin*t, amax*t
                    aratio = amax/amin
                    
                    amins[n] = amin
                    amaxs[n] = amax
                    aratios[n] = aratio
                    
                    if amin < amintol:
                        badamins.append((bid, i, j, k, amin))
                    if amax > amaxtol:
                        badamaxs.append((bid, i, j, k, amax))
                    if aratio > aratiotol:
                        badaratios.append((bid, i, j, k, aratio))
                    
                    n += 1
                
    ret = dict(
        amin   = {'descr':descr['amins'], 'values':amins, \
                  'bad':badamins, 'bb':len(badamins) },
        amax   = {'descr':descr['amaxs'], 'values':amaxs, \
                  'bad':badamaxs, 'bb':len(badamaxs)},
        aratio = {'descr':descr['aratios'], 'values':aratios, \
                  'bad':badaratios, 'bb':len(badaratios)}
    )
    
    # Save values
    if savepath is not None and not loaded:
        saveValues('%s/amin.%i' % (savepath, block.bid), amins)
        saveValues('%s/amax.%i' % (savepath, block.bid), amaxs)
        saveValues('%s/aratio.%i' % (savepath, block.bid), aratios)
        
        saveBad('%s/badamin.%i' % (savepath, block.bid), badamins)
        saveBad('%s/badamax.%i' % (savepath, block.bid), badamaxs)
        saveBad('%s/badaratio.%i' % (savepath, block.bid), badaratios)
    
    return ret
