__all__= [
    'Orthogonality',
    'Expansion',
]

def getModules():
    mods = []
    for m in __all__:
        mods.append( __import__(__name__+'.'+m, fromlist=[m]) )
    return mods
