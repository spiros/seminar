#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# This file is part of gridtool.
#
# gridtool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gridtool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with gridtool.  If not, see <http://www.gnu.org/licenses/>.

import sys, warnings, os
import math, numpy as np
from os.path import exists, join as pjoin
from hashlib import md5
from array import array
from Block import Block
from QualityTask import QualityTask

# Import memcom module
try:
    from memcomloader import memcom
except:
    print "Module MEMCOM failed to load"
    exit(1)

# Import matplotlib
try:
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    plot = True
except:
    print "PyPlot not found: no plots will be generated"
    plot = False


# Filter out deprecation warnings
warnings.filterwarnings('ignore', '.*', DeprecationWarning, 'memcom', 1121)

# Read command-line argument (database names)
if len(sys.argv) < 3:
    indbnames = ['grid.db']
else:
    indbnames = sys.argv[2:]

# No more than 4 databases
if len(indbnames) > 4:
    print "Warning: no more than 4 databases are supported!"
    print "Any other database will be ignored."
    print
    indbnames = indbnames[:4]

# Open databases and create directories
if not exists('Report'):
    os.mkdir ('Report')
dbs = {}
dbhashes = []
for i in indbnames:
    dbhash = md5(i).hexdigest()
    dbhashes.append(dbhash)
    dbs[dbhash] = memcom.db(i)
    if not exists('Report/' + dbhash):
        os.mkdir('Report/' + dbhash)

# Load quality measures
import Quality
measures = Quality.getModules()


results = []
for dbhash in dbhashes:
    db = dbs[dbhash]
    # Number of blocks
    bN = len(db.keys('COOR.*'))
    print "\n\nReading grid with %i blocks" % (bN)

    # Run quality check
    qt = QualityTask(dbhash, db, range(1, bN+1))
#    qt = QualityTask(dbhash, db, range(1, min(10, bN+1)))
    results.append(qt.run(measures))


# Plot results
if plot:
    # Set images path
    if not exists(pjoin('Report', 'img')):
        os.mkdir(pjoin('Report', 'img'))

    k = np.floor(np.log10(len(dbs))) + 1
    imgpath = pjoin('img', '%%s_%%0%ii.svg' % k)

    # Get result keys
    keys = results[0].keys()
    descrs = [results[0][k]['descr'] for k in keys]

    # Begin HTMl document
    widths = [0, 60, 45, 30, 23]
    html = '''
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <title>gridtool Report</title>
    <style type="text/css">
        h1 {
            text-align: center;
            margin-bottom: 50px;
        }

        h2 {
            margin-top: 50px;
        }

        .checkcase {
            text-align: center;
        }

        .checkimg {
            display: inline-block;
            width: %i%%;
        }

        .checkimg img {
            width: 100%%;
        }

        ul {
            text-align: left;
        }

        .badblocks li a {
            cursor: pointer;
            color: #3333FF;
        }

        .badcells {
            display: none;
        }
    </style>

    <script type="text/javascript">
        function showHideBC(bid) {
            s = document.getElementById(bid).style;
            if (s.display == 'block')
                s.display = 'none';
            else
                s.display = 'block';
        }
    </script>
</head>

<body>
    <h1>Grid Quality Report</h1>

'''% widths[len(dbs)]

    # Insert images
    for n, k in enumerate(keys):
        descr = descrs[n]
        html += '<hr /><div class="checkcase"><h2>' + descr + '</h2>'
        
        figures = []
        xlow, xhigh, ylow, yhigh = np.Inf, -np.Inf, np.Inf, -np.Inf

        for resID, res in enumerate(results, 0):
            # Get values
            values = res[k]['values']
            mean = np.mean(values)
            std = np.std(values)
            
            # Store values
            fs = open('Report/%s_%i.data' % (k, resID), 'wb')
            array('L', [len(values)]).tofile(fs)
            array('d', values).tofile(fs)
            fs.close()

            # Generate image
            imgname = imgpath % (k, resID)
            Nvalues = len(values)
            Nbins = min(40, round( Nvalues**.4 ))

            if 'xlabel' in res[k]:
                xlabel = res[k]['xlabel']
            else:
                xlabel = k

            # Generate pyplot figure
            f = plt.figure()
            figures.append((imgname, f))
            plt.hist(values, Nbins, normed=1, log=True, color='k')
            plt.xlabel(xlabel, fontsize='xx-large')
            plt.ylabel('Normalized number of cells', fontsize='xx-large')
            axis = f.get_axes()[0]
            [l.set_size('x-large') for l in \
             axis.get_xticklabels() + axis.get_yticklabels()]
            
            # Get limits
            myxlow, myxhigh = f.get_axes()[0].get_xlim()
            myylow, myyhigh = f.get_axes()[0].get_ylim()
            xlow = min(xlow, myxlow)
            xhigh = max(xhigh, myxhigh)
            ylow = min(ylow, myylow)
            yhigh = max(yhigh, myyhigh)

            # Update HTML document
            txt = 'Mean: %f<br />Standard deviation: %f' % (mean, std)
            alt = 'Histogram for ' + k
            html += '<div class="checkimg">'
            html += '<h3>%s</h3>' % indbnames[resID]
            html += '<p>Number of values: %i</p>' % len(values)
            html += '<p>%s</p><img src="%s" alt="%s" />' % (txt, imgname, alt)

            # Bad blocks
            html += '<ul class="badblocks">'
            
            # Find maximal number of blocks
            maxBN = 0
            for rres in results:
                bN = len(rres[k]['bb'])
                if bN > maxBN:
                    maxBN = bN
            
            # Write list
            for bid, bb in enumerate(res[k]['bb'], 1):

                # ulid: badcells_k_gridID_blockID
                ulid = 'badcells_%s_%i_%i' % (k, resID, bid)
                html += '<li><a onclick="showHideBC(\'%s\')">Block %i</a>: %i bad cells' % (ulid, bid, bb)

                # Write nested list: bad cells
                if bb < 100:
                    html += '<ul class="badcells" id="%s">' % ulid
                    
                    for bc in res[k]['bad']:
                        if bc[0] < bid:
                            continue
                        if bc[0] > bid:
                            break
                        html += '<li>ijk: (%i, %i, %i) - val: %f</li>' % bc[1:]
                    html += '</ul>'
                
                html += '</li>'

            # Write void items
            for i in xrange(maxBN - len(res[k]['bb'])):
                html += '<li style="visibility:hidden">&nbsp;</li>'

            html += '</ul>'

            html += '</div>'

        # Save figures
        for imgname, f in figures:
            axes = f.get_axes()[0]
            axes.set_xlim(xlow, xhigh)
            axes.set_ylim(ylow, yhigh)
            
            f.savefig(pjoin('Report', imgname), format='svg')
            
        
        
        html += '</div>'
    plt.show()

    # Close HTML document
    html += "</body></html>"
    fs = open(pjoin('Report', 'index.html'), 'w')
    fs.write(html)
    fs.close()
