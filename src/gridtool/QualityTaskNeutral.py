#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# This file is part of gridtool.
#
# gridtool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gridtool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with gridtool.  If not, see <http://www.gnu.org/licenses/>.

from os.path import join as pjoin
from Block import Block
import numpy as np
from array import array

class QualityTask:
    def __init__(self, inDir, outDir, *args):
        self.inDir = inDir
        self.savedir = outDir
        
        # Get blocks ids
        blocks = []
        for i in args:
            if type(i) == type(0):
                blocks.append(i)
            else:
                blocks += i
                
        # Uniquify
        s = set(blocks)
        self.blocks = list(s)
        
        
    def run(self, measures):
        result = {}
        
        for bid in self.blocks:
        
            # Open file
            fs = open(pjoin(self.inDir, 'block.%i'%bid), 'rb')
             
            # Get n1, n2, n3
            sizeArray = array('L')
            sizeArray.fromfile(fs, 3)
            ns = sizeArray.tolist()
            n1, n2, n3 = ns
            print "Block %i -- size: %ix%ix%i" % (bid, n1, n2, n3)
            
            # Get coordinates
            coorArray = array('d')
            coorArray.fromfile(fs, n1*n2*n3*3)
            coordinates = np.array(coorArray.tolist()).reshape(n1*n2*n3, 3)
            
            # Construct block
            b = Block(bid, ns, coordinates)
            
            # Measure
            parres = {}
            for m in measures:
                print '   -- Module ' + m.__name__ + ' on block ' + str(bid)
                
                # Compute 
                parres.update(m.measure(b, self.savedir))
            print
            
            # Concatenate results
            for k, v in parres.items():
                if k in result:
                    result[k]['values'] = \
                        np.concatenate((result[k]['values'], v['values']))
                    result[k]['bad'] += v['bad']
                    result[k]['bb'].append(v['bb'])
                else:
                    result[k] = v
                    result[k]['bb'] = [result[k]['bb']]
                    
        return result
