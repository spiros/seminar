#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# This file is part of gridtool.
#
# gridpro2memcom is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gridpro2memcom is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with gridpro2memcom.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import numpy.linalg as la
from Hexahedron import Hexahedron
    

class Block:
    def __init__(self, bid, dimensions, coordinates):
        """
        Initializes the data structure

        @param dimensions: the number of points along each axis
        @param coordinates: the n1*n2*n3-by-3 matrix of the coordinates
        """

        # Store dimensions and id
        self.bid = bid
        self.n1, self.n2, self.n3 = dimensions
        n1, n2, n3 = dimensions
        
        if n1*n2*n3 != coordinates.size/3:
            raise Exception('Coordinates size mismatch: %ix%ix%i  |  %i' % \
                            (n1,n2,n3, coordinates.size/3))

        # Store coordinates
        self.coordinates = np.zeros((n1, n2, n3, 3))
        kB = self.coordinates.nbytes / 1024.
        print "   -- This block occupies %f kB" % kB
        bn = 0
        for k in xrange(n3):
            for j in xrange(n2):
                for i in xrange(n1):
                    self.coordinates[i,j,k,:] = coordinates[bn, :]
                    bn += 1
                    
    def getHexahedron(self, i, j, k):
        c = self.coordinates
        vertices = [
            c[i,j,k  ,:], c[i+1,j,k  ,:], c[i+1,j+1,k  ,:], c[i,j+1,k  ,:],
            c[i,j,k+1,:], c[i+1,j,k+1,:], c[i+1,j+1,k+1,:], c[i,j+1,k+1,:]
        ]
        return Hexahedron(vertices)
        
    
    
