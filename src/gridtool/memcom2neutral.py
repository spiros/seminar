#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# This file is part of gridtool.
#
# gridtool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gridtool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with gridtool.  If not, see <http://www.gnu.org/licenses/>.

import sys, warnings, os
import numpy as np
from os.path import exists, join as pjoin
from array import array

# Import memcom module
try:
    from memcomloader import memcom
except:
    print "Module MEMCOM failed to load"
    exit(1)

# Filter out deprecation warnings
warnings.filterwarnings('ignore', '.*', DeprecationWarning, 'memcom', 1121)

# Create output directory
if exists('Grid'):
    print "Error: directory 'Grid' already exists."
    print "Exiting"
    exit(1)
os.mkdir('Grid')

# Open database
db = memcom.db('grid.db')

# Number of blocks
bN = len(db.keys('COOR.*'))

for bid in xrange(1, bN+1):             
    # Get n1, n2, n3
    temprt = db['MDES.' + str(bid)][:]
    ns = [temprt[s][0]+1 for s in ('N1', 'N2', 'N3')]
    n1, n2, n3 = ns

    # Get coordinates
    coordinates = db['COOR.' + str(bid)][:].reshape(n1*n2*n3 * 3)

    # Open file
    fs = open('Grid/block.%i' % bid, 'wb')

    # Store ns to file
    sizeArray = array('L', ns)
    sizeArray.tofile(fs)

    # Construct array and save to file
    blockArray = array('d', coordinates);
    blockArray.tofile(fs)
    fs.close()
    
    print 'Coordinates of block %i saved into %s' % (bid, fs.name)
    

