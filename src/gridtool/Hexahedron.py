#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# This file is part of gridtool.
#
# gridpro2memcom is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gridpro2memcom is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with gridpro2memcom.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import numpy.linalg as la

_angleMatrix = [
    (+ 1, - 4), (+ 1, - 5), (- 4, - 5),
    (- 1, + 2), (- 1, + 6), (+ 2, + 6),
    (- 2, + 3), (- 2, + 7), (+ 3, + 7),
    (- 3, + 4), (- 3, + 8), (+ 4, + 8),
    
    (- 5, + 9), (- 5, -12), (+ 9, -12),
    (- 6, - 9), (- 6, +10), (- 9, +10),
    (- 7, -10), (- 7, +11), (-10, +11),
    (- 8, -11), (- 8, +12), (-11, +12),
]


class Hexahedron:
    
    def __init__(self, vertices):
        self._vertices = vertices
    
    def _getVectors(self):
        if '_vectors' in self.__dict__:
            return self._vectors
    
        c = self._vertices
        v = [None]
        
        v.append( c[1] - c[0] );
        v.append( c[2] - c[1] );
        v.append( c[3] - c[2] );
        v.append( c[0] - c[3] );
        
        v.append( c[4] - c[0] );
        v.append( c[5] - c[1] );
        v.append( c[6] - c[2] );
        v.append( c[7] - c[3] );
        
        v.append( c[5] - c[4] );
        v.append( c[6] - c[5] );
        v.append( c[7] - c[6] );
        v.append( c[4] - c[7] );
        
        self._vectors = v
        return self._vectors
        
    def getAnglesMinMax(self):
        amax, amin = 0., np.pi
        v = self._getVectors()
        for (a1, a2) in _angleMatrix:
            v1 = np.sign(a1) * v[abs(a1)]
            v2 = np.sign(a2) * v[abs(a2)]
            
            angle = np.arccos( np.dot(v1,v2) / la.norm(v1) / la.norm(v2) )
            
            amax = max(amax, angle)
            amin = min(amin, angle)
        return amin, amax
    
    def getAngleRatio(self):
        amax, amin = 0., np.pi
        v = self._getVectors()
        for (a1, a2) in _angleMatrix:
            v1 = np.sign(a1) * v[abs(a1)]
            v2 = np.sign(a2) * v[abs(a2)]
            
            angle = np.arccos( np.dot(v1,v2) / la.norm(v1) / la.norm(v2) )
            
            amax = max(amax, angle)
            amin = min(amin, angle)
        return amax/amin
