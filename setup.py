#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# This file is part of gridtool.
#
# gridtool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# gridtool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with gridtool.  If not, see <http://www.gnu.org/licenses/>.

from os.path import abspath, dirname, join as pjoin
from distutils.core import setup

setupdir = dirname(abspath(__file__))
numdir = pjoin(setupdir, 'gridtool')

setup(name='Gridtool',
      version='1.0',
      description='Converter GridPro->MEMCOM and quality checker',
      author='Andrea Arteaga',
      author_email='arteagaa@student.ethz.ch',
      license='GPL-3',
      packages=['gridtool', 'gridtool.Quality'],
      package_dir={'gridtool': 'src/gridtool'},
      scripts=['gridtool']
     )

